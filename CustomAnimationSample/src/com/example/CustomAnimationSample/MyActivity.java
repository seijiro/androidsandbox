package com.example.CustomAnimationSample;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;


public class MyActivity extends Activity {

    private ImageView mCoinView;
    private boolean mIsHead;
    private Animation grow,shrink;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mCoinView = (ImageView)findViewById(R.id.image);
        mCoinView.setImageResource(R.drawable.coin);
        mIsHead = true;

////        shrink = new ScaleAnimation(1.0f,0.0f,1.0f,1.0f,
////                ScaleAnimation.RELATIVE_TO_SELF,0.5f,
////                ScaleAnimation.RELATIVE_TO_SELF,0.5f);
//
//        shrink.setDuration(150);
        shrink = AnimationUtils.loadAnimation(this,R.anim.shrink);
        shrink.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                if(mIsHead){
                    mIsHead = false;
                    mCoinView.setImageResource(R.drawable.tail);
                }
                else{
                    mIsHead = true;
                    mCoinView.setImageResource(R.drawable.coin);
                }
                mCoinView.startAnimation(grow);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        grow = AnimationUtils.loadAnimation(this,R.anim.grow);
//        grow = new ScaleAnimation(0.0f,1.0f,1.0f,1.0f,
//                ScaleAnimation.RELATIVE_TO_SELF,0.5f,
//                ScaleAnimation.RELATIVE_TO_SELF,0.5f);
//        grow.setDuration(150);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN){
            mCoinView.startAnimation(shrink);
            return true;
        }

        return super.onTouchEvent(event);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
