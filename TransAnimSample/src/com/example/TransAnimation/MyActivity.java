package com.example.TransAnimation;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

public class MyActivity extends Activity {

    private View mViewToAnim;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        final Button btn = (Button)findViewById(R.id.toggle);
        mViewToAnim = findViewById(R.id.screen);
    }

    public void trans(View v){
        if(mViewToAnim.getVisibility() == View.VISIBLE){
            //Viewが可視の場合みぎにスライド
            final Animation out = AnimationUtils.loadAnimation(this,android.R.anim.fade_out);
            mViewToAnim.startAnimation(out);
            mViewToAnim.setVisibility(View.INVISIBLE);
        }
        else{
            final Animation in = AnimationUtils.loadAnimation(this,android.R.anim.fade_in);
            mViewToAnim.startAnimation(in);
            mViewToAnim.setVisibility(View.VISIBLE);
        }

    }
}
