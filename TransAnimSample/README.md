FadeIn、FadeOut
テスト
==============
フェードイン、フェードアウトアニメーションの実装

これだけでできるなんて！知らなかった。


```

        if(mViewToAnim.getVisibility() == View.VISIBLE){
            //Viewが可視の場合フェードアウト
            final Animation out = AnimationUtils.loadAnimation(this,android.R.anim.fade_out);
            mViewToAnim.startAnimation(out);
            mViewToAnim.setVisibility(View.INVISIBLE);
        }
        else{
            final Animation in = AnimationUtils.loadAnimation(this,android.R.anim.fade_in);
            mViewToAnim.startAnimation(in);
            mViewToAnim.setVisibility(View.VISIBLE);
        }

```

